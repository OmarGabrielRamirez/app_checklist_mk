import 'package:flutter/material.dart';

  final _icons = <String, IconData> {
   'format_list_numbered' : Icons.format_list_numbered,
  };

  Icon getIcon (String nameIcon){
    return Icon(_icons[nameIcon], color: Colors.orange, size: 40.0,);
 }