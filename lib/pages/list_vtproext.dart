import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:quiver/collection.dart';
import 'package:http/http.dart' as http;

class CheckListVitrinaPrExt extends StatefulWidget {
  @override
  _generarVitrinaPrExtPage createState() => _generarVitrinaPrExtPage();
}

class _generarVitrinaPrExtPage extends State<CheckListVitrinaPrExt> {
  bool selected = false;
  String mapValues;
  int item = 0, total = 0, itemsCriticios = 0, itemsNormales = 0;
  List<Map<String, String>> itemsValues = [];
  Map<String, String> values;
  int itemsCriticos = 0;

  List valuesCheck = [];
  final valCheck = Map<String, bool>();
  var mapM = Multimap<String, int>();
  var details = Map();

  var itemsStatus = List<bool>();

  Future<Map<String, bool>> getVal() async {
    var res = await http.get(
        Uri.encodeFull("https://intranet.prigo.com.mx/api/getitemsvtpext"),
        headers: {"Accept": "application/json"});
    var data = json.decode(res.body);
    final List<dynamic> data2 = json.decode(res.body);
    for (var x in data2) {
      var itemCheckVpExt = {
        'idItem': x['idItem'],
        'critico': x['critico'],
        'status': false
      };
      valuesCheck.add(itemCheckVpExt);
    }
    for (var i in data) {
      if (i["critico"] == 1) {
        itemsCriticos++;
      } else if (i["critico"] == 0) {
        itemsNormales++;
      }
      details.addAll(
          {"idItem": i["idItem"], "critico": i["critico"], "status": false});

      valCheck.putIfAbsent('"' + i["idItem"].toString() + '"', () => false);
    }

    return valCheck;
  }

  Future<List<Item>> getData() async {
    var res = await http.get(
        Uri.encodeFull("https://intranet.prigo.com.mx/api/getitemsvtpext"),
        headers: {"Accept": "application/json"});
    var data = json.decode(res.body);
    List<Item> items = [];
    for (var i in data) {
      Item item = Item(i["idItem"], i["accion"], i["critico"], i["puntaje"]);
      items.add(item);
      itemsStatus.add(false);
    }
    return items;
  }

  @override
  void initState() {
    super.initState();
    this.getVal();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Stack(
        overflow: Overflow.visible,
        alignment: FractionalOffset(.5, 1.2),
        children: [
          Container(        
            height: 50.0,
            color: Colors.orange,
          ),
          Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: Container(
                  child: Text(
                "Total: " + item.toString() + " / " + total.toString(),
                style: TextStyle(fontFamily: 'Lato', fontSize: 25, color: Colors.white, fontWeight: FontWeight.w500),
              ))),
        ],
      ),
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: Icon(
          Icons.format_list_numbered,
          color: Colors.white,
          size: 16,
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.arrow_forward,
                color: Colors.white,
              ),
              onPressed: () {
                saveResp(item, itemsCriticos, itemsNormales, mapValues);
                Navigator.pushNamed(context, 'check_vp_int');
              })
        ],
        title: Text(
          "Vitrina Profunda - Vitrina exterior",
          style:
              TextStyle(fontFamily: 'Lato', color: Colors.white, fontSize: 17),
        ),
      ),
      body: _contentItems(),
    );
  }

  Widget _contentItems() {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 20, left: 20, right: 20),
          child: FutureBuilder(
              future: getData(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.data == null) {
                  return Container(
                      child: Center(
                    child: Text("Cargando...",
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                  ));
                } else {
                  total = snapshot.data.length;
                  Widget icon;
                  return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        if (snapshot.data[index].critico == 1) {
                          icon = Icon(
                            Icons.assistant_photo,
                            size: 17,
                            color: Colors.red,
                          );
                        } else if (snapshot.data[index].critico == 0) {
                          icon = Icon(
                            Icons.assistant_photo,
                            size: 17,
                            color: Colors.orange,
                          );
                        }
                        return ListTile(
                          leading: icon,
                          title: Text(
                            snapshot.data[index].descripcion,
                            style: TextStyle(
                                fontFamily: 'Lato',
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                color: Colors.black),
                          ),
                          trailing: Checkbox(
                              value: itemsStatus[index],
                              onChanged: (bool val) {
                                if (val == true) {
                                  item++;
                                  if (snapshot.data[index].critico == 1) {
                                    itemsCriticos--;
                                  } else if (snapshot.data[index].critico ==
                                      0) {
                                    itemsNormales--;
                                  }
                                } else if (val == false) {
                                  item--;
                                  if (snapshot.data[index].critico == 1) {
                                    itemsCriticos++;
                                  } else if (snapshot.data[index].critico ==
                                      0) {
                                    itemsNormales++;
                                  }
                                }
                                setState(() {
                                  valCheck.update(
                                      '"' +
                                          snapshot.data[index].index
                                              .toString() +
                                          '"',
                                      (existingValue) => val);
                                  mapValues = json.encode(valCheck.toString());
                                  itemsStatus[index] = !itemsStatus[index];
                                });
                              }),
                        );
                      });
                }
              }),
        ),
      ],
    );
  }
}

saveResp(int item, int itemsCriticos, int itemsNormales, String valChec) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  sharedPreferences.setInt("TotalSlctVpExt", item);
  sharedPreferences.setInt("CriticosSlctVpExt", itemsCriticos);
  sharedPreferences.setInt("NormalesSlctVpExt", itemsNormales);
  sharedPreferences.setString("respCheckVpExt", valChec);
}

class Item {
  final int index;
  final String descripcion;
  final int critico;
  final int puntaje;

  Item(this.index, this.descripcion, this.critico, this.puntaje);
}
