import 'package:app_operativa/providers/index_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ChecklistPage extends StatefulWidget {
  @override
  _ChecklistScreenState createState() => _ChecklistScreenState();
}

class _ChecklistScreenState extends State<ChecklistPage> {
  @override
  void initState() {
    super.initState();
  }

  notRotate() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    notRotate();
    return Scaffold(
      appBar: AppBar(
        title: Text("Checklist",
            style: TextStyle(
                fontFamily: 'Lato',
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 17)),
        leading:
            Icon(Icons.format_list_numbered, color: Colors.white, size: 16.0),
      ),
      body: Container(
        child: _items(),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.orange,
        child: Icon(Icons.first_page, color: Colors.white),
        onPressed: () {
          Navigator.pushReplacementNamed(context, '/');
        },
      ),
    );
  }

  Widget _items() {
    return FutureBuilder(
      future: MenuProvider.loadingDataCheck(),
      initialData: [],
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          padding: EdgeInsets.fromLTRB(10.0, 10, 10.0, 10.0),
          children: _itemsList(snapshot.data, context),
        );
      },
    );
  }

  List<Widget> _itemsList(List<dynamic> data, BuildContext context) {
    final List<Widget> itmMenu = [];
    data.forEach((opt) {
      final widgetTmp = GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, opt['route']);
        },
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          margin: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 13, right: 13),
          elevation: 2.0,
          child: Container(
            margin: EdgeInsets.only(top: 30.0, bottom: 30.0),
            child: Column(children: <Widget>[
              Text(opt['title'],
                  style: TextStyle(
                      fontFamily: 'Lato',
                      color: Colors.orange,
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold)),
              SizedBox(
                width: 20.0,
                height: 10.0,
              ),
              Container(
                child: Icon(
                  Icons.check_circle_outline,
                  color: Colors.orange,
                  size: 30.0,
                ),
              )
            ]),
          ),
        ),
      );
      itmMenu..add(widgetTmp);
    });
    return itmMenu;
  }
}
