import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

int criticosVpInt = 0,
    normalesVpInt = 0,
    criticosVpExt = 0,
    normalesVpExt = 0,
    totalNormales = 0,
    totalCriticos = 0,
    puntajeCritico = 0,
    puntajeNormal = 0,
    puntajePosible = 0,
    puntajeFinal = 0,
    count = 0;

class CheckListVpIntePage extends StatefulWidget {
  @override
  _checkListVpIntePage createState() => _checkListVpIntePage();
}

class _checkListVpIntePage extends State<CheckListVpIntePage> {
  String mapValues;
  bool selected = false;
  int item = 0, total = 0, itemsCriticos = 0, itemsNormales = 0;
  List<Map<String, String>> itemsValues = [];
  var itemsStatus = List<bool>();
  final valCheck = Map<String, bool>();
  List valuesCheck = [];
  var details = Map();

  _openGallery() {}

  _openCamera() {}

  Future<void> _showSelectionImage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Selecciona una opción",
                style: TextStyle(fontFamily: 'Lato', fontSize: 18)),
            content: SingleChildScrollView(
                child: ListBody(children: <Widget>[
              GestureDetector(
                child: Text("Galeria",
                    style: TextStyle(fontFamily: 'Lato', fontSize: 16)),
                onTap: () {
                  _openGallery();
                },
              ),
              GestureDetector(
                child: Text("Cámara",
                    style: TextStyle(fontFamily: 'Lato', fontSize: 16)),
                onTap: () {
                  _openCamera();
                },
              ),
            ])),
          );
        });
  }

  setValues() {
    criticosVpInt = 0;
    normalesVpInt = 0;
    criticosVpExt = 0;
    normalesVpExt = 0;
    totalNormales = 0;
    totalCriticos = 0;
    puntajeCritico = 0;
    puntajeNormal = 0;
    puntajePosible = 0;
    puntajeFinal = 0;
    count = 0;
  }

  Future<Map<String, bool>> getVal() async {
    var res = await http.get(
        Uri.encodeFull("https://intranet.prigo.com.mx/api/getitemsvtpint"),
        headers: {"Accept": "application/json"});
    var data = json.decode(res.body);
    final List<dynamic> data2 = json.decode(res.body);
    for (var x in data2) {
      var itemCheckVpExt = {
        'idItem': x['idItem'],
        'critico': x['critico'],
        'status': false
      };
      valuesCheck.add(itemCheckVpExt);
    }
    for (var i in data) {
      if (i["critico"] == 1) {
        itemsCriticos++;
      } else if (i["critico"] == 0) {
        itemsNormales++;
      }
      details.addAll(
          {"idItem": i["idItem"], "critico": i["critico"], "status": false});

      valCheck.putIfAbsent('"' + i["idItem"].toString() + '"', () => false);
    }

    return valCheck;
  }

  Future<List<Item>> getData() async {
    var res = await http.get(
        Uri.encodeFull("https://intranet.prigo.com.mx/api/getitemsvtpint"),
        headers: {"Accept": "application/json"});
    var data = json.decode(res.body);
    List<Item> items = [];
    for (var i in data) {
      Item item = Item(i["idItem"], i["accion"], i["critico"], i["puntaje"]);
      items.add(item);
      itemsStatus.add(false);
    }
    return items;
  }

  @override
  void initState() {
    this.getData();
    this.getVal();
    this.getPuntajeFinally();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: Stack(
        overflow: Overflow.visible,
        alignment: FractionalOffset(.5, 1.0),
        children: [
          Container(
            height: 50.0,
            color: Colors.orange,
          ),
          Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: Container(
                  child: Text(
                "Total: " + item.toString() + " / " + total.toString(),
                style: TextStyle(
                    fontFamily: 'Lato',
                    fontSize: 25,
                    color: Colors.white,
                    fontWeight: FontWeight.w500),
              ))),
        ],
      ),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        actions: <Widget>[
          GestureDetector(
              onTap: () {
                saveResp(item, itemsCriticos, itemsNormales, mapValues);
                Navigator.pushNamed(context, 'images_vp');
              },
              child: Container(
                padding: EdgeInsets.all(12),
                alignment: Alignment.center,
                child: Icon(
                  Icons.arrow_forward,
                ),
              )),
        ],
        title: Text(
          "Vitrina Profunda - Vitrina Interior",
          style:
              TextStyle(fontFamily: 'Lato', color: Colors.white, fontSize: 17),
        ),
      ),
      body: _contentItems(),
    );
  }

  Widget _contentItems() {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 20, left: 20, right: 20),
          child: FutureBuilder(
              future: getData(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.data == null) {
                  return Container(
                      child: Center(
                    child: Text("Cargando...",
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                  ));
                } else {
                  total = snapshot.data.length;
                  Widget icon;
                  return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        if (snapshot.data[index].critico == 1) {
                          icon = Icon(
                            Icons.assistant_photo,
                            size: 17,
                            color: Colors.red,
                          );
                        } else if (snapshot.data[index].critico == 0) {
                          icon = Icon(
                            Icons.assistant_photo,
                            size: 17,
                            color: Colors.orange,
                          );
                        }
                        return ListTile(
                          leading: icon,
                          title: Text(
                            snapshot.data[index].descripcion,
                            style: TextStyle(
                              fontFamily: 'Lato',
                              fontSize: 18,
                            ),
                          ),
                          trailing: Checkbox(
                              value: itemsStatus[index],
                              onChanged: (bool val) {
                                if (val == true) {
                                  item++;
                                  if (snapshot.data[index].critico == 1) {
                                    itemsCriticos--;
                                  } else if (snapshot.data[index].critico ==
                                      0) {
                                    itemsNormales--;
                                  }
                                } else if (val == false) {
                                  item--;
                                  if (snapshot.data[index].critico == 1) {
                                    itemsCriticos++;
                                  } else if (snapshot.data[index].critico ==
                                      0) {
                                    itemsNormales++;
                                  }
                                }
                                setState(() {
                                  valCheck.update(
                                      '"' +
                                          snapshot.data[index].index
                                              .toString() +
                                          '"',
                                      (existingValue) => val);
                                  mapValues = json.encode(valCheck.toString());
                                  itemsStatus[index] = !itemsStatus[index];
                                });
                              }),
                        );
                      });
                }
              }),
        ),
      ],
    );
  }

  Widget _alertWar() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Icon(Icons.check_circle_outline,
                color: Colors.orange, size: 70.0),
            content: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Text("Los datos del checklist se guardarán.",
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 17.0,
                      fontFamily: 'Lato')),
            ]),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    saveResp(item, itemsCriticos, itemsNormales, mapValues);
                    Navigator.pushNamed(context, 'calificacion_vp');
                  },
                  child: Text(
                    "Continuar",
                    style: TextStyle(
                        color: Colors.orange,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Lato'),
                  )),
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    "Cancelar",
                    style: TextStyle(
                        color: Colors.orange,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Lato'),
                  ))
            ],
          );
        });
  }

  getPuntajeFinally() async {
    var res = await http.get(
        Uri.encodeFull("https://intranet.prigo.com.mx/api/getcalvtp"),
        headers: {"Accept": "application/json"});
    var data = json.decode(res.body);
    for (var i in data) {
      puntajePosible += i['puntaje'];
      puntajePosible = puntajePosible;
    }

    return puntajeFinal;
  }

  saveResp(
      int item, int itemsCriticos, int itemsNormales, String valChec) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setInt("TotalSlctVpInt", item);
    sharedPreferences.setInt("CriticosSlcVpInt", itemsCriticos);
    sharedPreferences.setInt("NormalesSlctVpInt", itemsNormales);
    sharedPreferences.setString("respCheckVpInt", valChec);

    normalesVpInt = sharedPreferences.getInt("NormalesSlctVpInt");
    normalesVpExt = sharedPreferences.getInt("NormalesSlctVpExt");
    criticosVpInt = sharedPreferences.getInt("CriticosSlcVpInt");
    criticosVpExt = sharedPreferences.getInt("CriticosSlctVpExt");

    setState(() {
      totalCriticos = criticosVpInt + criticosVpExt;
      totalNormales = normalesVpInt + normalesVpExt;
      puntajeCritico = totalCriticos * 5;
      puntajeNormal = totalNormales * 2;
      int aux = puntajeCritico + puntajeNormal;
      puntajeFinal = puntajePosible - aux;
    });

    String fechaIngresada = sharedPreferences.getString("dateUser");
    String fechaGenerada = sharedPreferences.getString("dateAuto");
    String idSucursal = sharedPreferences.getString("idSuc");
    String responsable = sharedPreferences.getString("resp");
    String checklistVpExt = sharedPreferences.getString("respCheckVpExt");
    String checklistVpInt = sharedPreferences.getString("respCheckVpInt");

    Map data = {
      'idTipoCheckList': "CHVT",
      'responsable': responsable,
      'idSuc': idSucursal,
      'fechaGenerada': fechaGenerada,
      'fechaIngresada': fechaIngresada,
      'puntajeFinal': puntajeFinal.toString(),
      'puntajeCritico': puntajeCritico.toString(),
      'puntajeNormal': puntajeNormal.toString(),
      'RespChecklistVpExt': checklistVpExt,
      'RespChecklistVpInt': checklistVpInt
    };

    Map<String, String> headers = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
    };

    var response = await http.post(
        "https://intranet.prigo.com.mx/api/savecheckvp",
        body: data,
        headers: headers);
    print(response.body);
    setValues();
  }
}

class Item {
  final int index;
  final String descripcion;
  final int critico;
  final int puntaje;

  Item(this.index, this.descripcion, this.critico, this.puntaje);
}
