import 'package:app_operativa/models/check_model.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:app_operativa/pages/details_vp.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class VitrinaProPage extends StatefulWidget {
  @override
  _vProfundaPage createState() => _vProfundaPage();
}

class _vProfundaPage extends State<VitrinaProPage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  List<Check> _checks = List<Check>();
  List<Check> _check1 = List<Check>();

  Color _color = Colors.black38;
  Color _iconColor = Colors.black38;
  Color _textColor = Colors.black54;
  double _widthBorder = 1.1;
  String _date = "Buscar fecha";
  String _sucursal;
  String _fecha;
  String _responsable;
  String formattedDate;
  Future<List<Check>> getCheck() async {
    var url = "https://intranet.prigo.com.mx/api/getcheckvp";
    var response = await http.get(url);
    var allChecks = List<Check>();

    if (response.statusCode == 200) {
      var checks = json.decode(response.body);
      for (var check in checks) {
        allChecks.add(Check.fromJson(check));
      }
    }
    return allChecks;
  }

  Icon setIcon(puntajeFinal) {
    if (puntajeFinal > 30) {
      return Icon(
        Icons.check_circle_outline,
        color: Colors.green,
        size: 38.0,
      );
    } else if (puntajeFinal > 20) {
      return Icon(
        Icons.check_circle_outline,
        color: Colors.orange,
        size: 38.0,
      );
    } else if (puntajeFinal < 20) {
      return Icon(Icons.close, color: Colors.red, size: 38.0);
    }
  }

  Text setPuntaje(puntajeFinal) {
    if (puntajeFinal > 30) {
      return Text(
        puntajeFinal.toString() + " / 48",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
            color: Colors.green,
            fontFamily: 'lato'),
      );
    } else if (puntajeFinal > 20) {
      return Text(
        puntajeFinal.toString() + " / 48",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
            color: Colors.orange,
            fontFamily: 'lato'),
      );
    } else if (puntajeFinal < 20) {
      return Text(
        puntajeFinal.toString() + " / 48",
        style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.bold,
            color: Colors.red,
            fontFamily: 'lato'),
      );
    }
  }

  @override
  void initState() {
    getCheck().then((value) {
      setState(() {
        _checks.addAll(value);
        _check1 = _checks;
      });
    });
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _content(),
      appBar: AppBar(
          title: Text("Vitrina Profunda",
              style: TextStyle(
                  fontFamily: 'Lato',
                  fontWeight: FontWeight.w500,
                  fontSize: 17,
                  color: Colors.white)),
          iconTheme:
              IconThemeData(color: Colors.white, size: 16, opacity: 0.9)),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, 'n_vprofunda');
          },
          backgroundColor: Colors.orange,
          child: Icon(Icons.add, size: 30, color: Colors.white)),
    );
  }

  Widget _content() {
    return ListView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(vertical: 1, horizontal: 10),
        itemCount: _check1.length + 1,
        itemBuilder: (context, index) {
          return index == 0 ? _searchBar() : _listChecks(index - 1);
        });
  }

  _listChecks(index) {
    String fecha = _check1[index].fecha.toString();
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => DetailsPage(_check1[index].id.toString())));
      },
      child: Padding(
        padding: EdgeInsets.only(left: 8, right: 8, top: 5, bottom: 5),
        child: Card(
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.elliptical(10, 60))),
          elevation: 5.0,
          child: Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 20, top: 5, right: 5),
                          alignment: Alignment.centerLeft,
                          child:
                              Icon(Icons.place, size: 17, color: Colors.orange),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 5),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            _check1[index].sucursal,
                            style: TextStyle(
                              fontFamily: 'Lato',
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Colors.orange,
                              shadows: [
                                Shadow(
                                  blurRadius: 0.1,
                                  color: Colors.black,
                                  offset: Offset(0.1, 0.5),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 20, top: 5, right: 5),
                          alignment: Alignment.centerLeft,
                          child: Icon(Icons.date_range,
                              size: 17, color: Colors.orange),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 5),
                          alignment: Alignment.centerLeft,
                          child: Text(fecha.substring(0, 18 - 2),
                              style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black,
                                  shadows: [
                                    Shadow(
                                      blurRadius: 1.5,
                                      color: Colors.white,
                                      offset: Offset(1.0, 0.5),
                                    ),
                                  ],
                                  fontFamily: 'Lato')),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 20, top: 5, right: 5),
                          alignment: Alignment.centerLeft,
                          child: Icon(Icons.assessment,
                              size: 17, color: Colors.orange),
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 5),
                            child: setPuntaje(_check1[index].puntajeFinal))
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 20, top: 5, right: 5),
                          alignment: Alignment.centerLeft,
                          child: Icon(Icons.perm_identity,
                              size: 17, color: Colors.orange),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 1),
                          alignment: Alignment.centerLeft,
                          child: Text(_check1[index].responsable,
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.w200,
                                  color: Colors.white,
                                  shadows: [
                                    Shadow(
                                      blurRadius: 0.4,
                                      color: Colors.black,
                                      offset: Offset(1.0, 1.0),
                                    ),
                                  ],
                                  fontFamily: 'Lato')),
                        ),
                      ],
                    ),
                  ]),
                ),
                Container(
                  padding:
                      EdgeInsets.only(left: 30, top: 10, bottom: 10, right: 15),
                  alignment: Alignment.topLeft,
                  child: setIcon(_check1[index].puntajeFinal),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _searchBar() {
    return Column(
      children: <Widget>[
        Padding(
            padding:
                const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 5),
            child: Container(
              height: 50,
              child: TextField(
                cursorColor: Colors.orange,
                style: TextStyle(fontFamily: 'Lato'),
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(color: Colors.orange),
                    ),
                    prefixIcon: Icon(Icons.place),
                    labelText: 'Buscar sucursal',
                    hintText: 'K Manacar',
                    hintStyle: TextStyle(
                      fontFamily: 'Lato',
                      fontSize: 14.0,
                    ),
                    labelStyle: TextStyle(
                        fontFamily: 'Lato',
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold)),
                onChanged: (text) {
                  text = text.toLowerCase();
                  setState(() {
                    _sucursal = text.toLowerCase();
                    _check1 = _checks.where((check) {
                      var checkSuc = check.sucursal.toLowerCase() +
                          check.fecha.toLowerCase().substring(0, 19 - 9);
                      if (_date == "Buscar fecha") {
                        _fecha = "";
                      } else {
                        _fecha = _date;
                      }
                      return checkSuc.contains(text + _fecha);
                    }).toList();
                  });
                },
              ),
            )),
        Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
          Padding(padding: EdgeInsets.only(top: 10, left: 10)),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(),
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  border: Border.all(width: _widthBorder, color: _color)),
              height: 50,
              child: Row(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.only(left: 10, right: 0),
                      child: Icon(Icons.date_range, color: _iconColor)),
                  Container(
                    child: FlatButton(
                      onPressed: () {
                        DatePicker.showDatePicker(
                          context,
                          showTitleActions: true,
                          minTime: DateTime(2020, 1, 1, 12, 00),
                          maxTime: DateTime(2025, 12, 12, 24, 00),
                          onChanged: (date) {},
                          onConfirm: (date) {
                            formattedDate =
                                DateFormat('yyyy-dd-MM').format(date);
                            setState(() {
                              _date = formattedDate;
                              _color = Colors.black38;
                              _iconColor = Colors.black38;
                              _widthBorder = 1.1;
                              _textColor = Colors.black54;
                              _check1 = _checks.where((check) {
                                var checkSuc = check.fecha
                                        .toLowerCase()
                                        .substring(0, 19 - 9) +
                                    check.sucursal.toLowerCase();
                                if (_sucursal == null) {
                                  _sucursal = "";
                                } else {
                                  _sucursal = _sucursal;
                                }
                                return checkSuc.contains(
                                    formattedDate.toLowerCase() + _sucursal);
                              }).toList();
                            });
                          },
                          currentTime: DateTime.now(),
                          locale: LocaleType.es,
                        );
                        setState(() {
                          _color = Colors.orange;
                          _iconColor = Colors.orange;
                          _widthBorder = 2.0;
                          _textColor = Colors.orange;
                        });
                      },
                      child: Text(
                        _date,
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            color: _textColor),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              height: 50,
              padding: EdgeInsets.only(left: 5, right: 10),
              child: TextField(
                cursorColor: Colors.orange,
                style: TextStyle(fontFamily: 'Lato'),
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(color: Colors.orange),
                    ),
                    prefixIcon: Icon(Icons.perm_identity),
                    labelText: 'Buscar responsable',
                    labelStyle: TextStyle(
                        fontFamily: 'Lato',
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold)),
                onChanged: (text) {
                  text = text.toLowerCase();
                  setState(() {
                    _check1 = _checks.where((check) {
                      var checkSuc = check.responsable.toLowerCase();
                      return checkSuc.contains(text);
                    }).toList();
                  });
                },
              ),
            ),
          ),
        ]),
      ],
    );
  }
}


