import 'package:app_operativa/pages/details_vp.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:app_operativa/models/check_model.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

class VitrinaProPageIndex extends StatefulWidget {
  @override
  _vProfundaPage createState() => _vProfundaPage();
}

class _vProfundaPage extends State<VitrinaProPageIndex> {
  List<Check> _checks = List<Check>();
  List<Check> _check1 = List<Check>();
  List dataSucursales = List();
  String _date = "Buscar fecha",
      _sucursal,
      _fecha,
      formattedDate,
      _selection = null,
      texto;
  double _widthBorder = 1.1;
  Color _color = Colors.black38,
      _iconColor = Colors.black38,
      _textColor = Colors.black54;

  Future<List<Check>> getCheck() async {
    var response =
        await http.get("https://intranet.prigo.com.mx/api/getcheckvp");
    var allChecks = List<Check>();

    if (response.statusCode == 200) {
      var checks = json.decode(response.body);
      for (var check in checks) {
        allChecks.add(Check.fromJson(check));
      }
    }
    return allChecks;
  }

  Future<String> getData() async {
    var res = await http.get(
        Uri.encodeFull("https://intranet.prigo.com.mx/api/checksucursales"),
        headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      dataSucursales = resBody;
    });
  }

  @override
  void initState() {
    this.getData();
    getCheck().then((value) {
      setState(() {
        _checks.addAll(value);
        _check1 = _checks;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: choices.length,
        child: Scaffold(
          floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.pushNamed(context, 'n_vprofunda');
              },
              backgroundColor: Colors.orange,
              child: Icon(Icons.add, size: 30, color: Colors.white)),
          appBar: AppBar(
            backgroundColor: Colors.orange,
            title: Text("Checklist - Vitrina Profunda",
                style: TextStyle(
                    fontFamily: 'Lato',
                    fontWeight: FontWeight.w500,
                    fontSize: 17,
                    color: Colors.white)),
            bottom: TabBar(
              labelColor: Colors.white,
              indicatorColor: Colors.white,
              isScrollable: false,
              tabs: choices.map<Widget>((ItemTab choice) {
                return Tab(
                  child: Container(
                      child: Column(
                    children: <Widget>[
                      Container(
                        child: Text(
                          choice.title,
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'Lato',
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 3),
                        child: Icon(
                          choice.icon,
                          size: 20,
                        ),
                      )
                    ],
                  )),
                );
              }).toList(),
            ),
          ),
          body: TabBarView(children: [
            Container(child: _content()),
            Card(
                child: Container(
              child: _content(),
            )),
          ]),
        ),
      ),
    );
  }

  Icon setIcon(puntajeFinal) {
    if (puntajeFinal > 30) {
      return Icon(
        Icons.check_circle_outline,
        color: Colors.green,
        size: 38.0,
      );
    } else if (puntajeFinal > 20) {
      return Icon(
        Icons.check_circle_outline,
        color: Colors.orange,
        size: 38.0,
      );
    } else if (puntajeFinal < 20) {
      return Icon(Icons.close, color: Colors.red, size: 38.0);
    }
  }

  Text setPuntaje(puntajeFinal) {
    if (puntajeFinal > 30) {
      return Text(
        puntajeFinal.toString() + " / 48",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
            color: Colors.green,
            fontFamily: 'lato'),
      );
    } else if (puntajeFinal > 20) {
      return Text(
        puntajeFinal.toString() + " / 48",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
            color: Colors.orange,
            fontFamily: 'lato'),
      );
    } else if (puntajeFinal < 20) {
      return Text(
        puntajeFinal.toString() + " / 48",
        style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.bold,
            color: Colors.red,
            fontFamily: 'lato'),
      );
    }
  }

  Widget _content() {
    return ListView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(vertical: 1, horizontal: 10),
        itemCount: _check1.length + 1,
        itemBuilder: (context, index) {
          return index == 0 ? _searchBar() : _listChecks(index - 1);
        });
  }

  _listChecks(index) {
    String fecha = _check1[index].fecha.toString();
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => DetailsPage(_check1[index].id.toString())));
      },
      child: Padding(
        padding: EdgeInsets.only(left: 8, right: 8, top: 5, bottom: 5),
        child: Card(
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.elliptical(10, 60))),
          elevation: 5.0,
          child: Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 20, top: 5, right: 5),
                          alignment: Alignment.centerLeft,
                          child:
                              Icon(Icons.place, size: 17, color: Colors.orange),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 5),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            _check1[index].sucursal,
                            style: TextStyle(
                              fontFamily: 'Lato',
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Colors.orange,
                              shadows: [
                                Shadow(
                                  blurRadius: 0.1,
                                  color: Colors.black,
                                  offset: Offset(0.1, 0.5),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 20, top: 5, right: 5),
                          alignment: Alignment.centerLeft,
                          child: Icon(Icons.date_range,
                              size: 17, color: Colors.orange),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 5),
                          alignment: Alignment.centerLeft,
                          child: Text(fecha.substring(0, 18 - 2),
                              style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black,
                                  shadows: [
                                    Shadow(
                                      blurRadius: 1.5,
                                      color: Colors.white,
                                      offset: Offset(1.0, 0.5),
                                    ),
                                  ],
                                  fontFamily: 'Lato')),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 20, top: 5, right: 5),
                          alignment: Alignment.centerLeft,
                          child: Icon(Icons.assessment,
                              size: 17, color: Colors.orange),
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 5),
                            child: setPuntaje(_check1[index].puntajeFinal))
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 20, top: 5, right: 5),
                          alignment: Alignment.centerLeft,
                          child: Icon(Icons.perm_identity,
                              size: 17, color: Colors.orange),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 1),
                          alignment: Alignment.centerLeft,
                          child: Text(_check1[index].responsable,
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.w200,
                                  color: Colors.white,
                                  shadows: [
                                    Shadow(
                                      blurRadius: 0.4,
                                      color: Colors.black,
                                      offset: Offset(1.0, 1.0),
                                    ),
                                  ],
                                  fontFamily: 'Lato')),
                        ),
                      ],
                    ),
                  ]),
                ),
                Container(
                  padding:
                      EdgeInsets.only(left: 30, top: 10, bottom: 10, right: 15),
                  alignment: Alignment.topLeft,
                  child: setIcon(_check1[index].puntajeFinal),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _searchBar() {
    return Column(
      children: <Widget>[
        Padding(
          padding:
              const EdgeInsets.only(top: 10, bottom: 5, right: 10, left: 10),
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(width: 2, color: Colors.orange),
                borderRadius: BorderRadius.all(Radius.circular(8.0))),
            height: 50,
            child: DropdownButton(
                underline: Container(color: Colors.white),
                isExpanded: true,
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: Colors.orange,
                ),
                items: dataSucursales.map((item) {
                  return DropdownMenuItem(
                      value: item['idMicros'],
                      child: Row(
                        children: <Widget>[
                          SizedBox(width: 10),
                          Icon(Icons.place, size: 20.0, color: Colors.orange),
                          SizedBox(width: 12),
                          Text(
                            item['idMicros'],
                            style: TextStyle(
                                fontFamily: 'Lato',
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                                color: Colors.black54),
                          ),
                        ],
                      ));
                }).toList(),
                onChanged: (newVal) {
                  texto = newVal.toString().toLowerCase().replaceAll(" ", "");
                  setState(() {
                    _selection = newVal;
                    _check1 = _checks.where((check) {
                      var checkSuc =
                          check.sucursal.toLowerCase().replaceAll(" ", "");
                      return checkSuc.contains(texto);
                    }).toList();
                    // _selection = null;
                  });
                },
                value: _selection != null ? _selection : null,
                hint: Container(
                    child: Row(
                  children: <Widget>[
                    SizedBox(width: 10),
                    Icon(Icons.place, color: Colors.orange),
                    SizedBox(width: 10),
                    Text(
                      'Selecciona una sucursal',
                      style: TextStyle(
                          fontFamily: 'Lato',
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ))),
          ),
        ),
        Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
          Padding(padding: EdgeInsets.only(top: 10, left: 10)),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(),
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  border: Border.all(width: _widthBorder, color: _color)),
              height: 50,
              child: Row(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.only(left: 10, right: 0),
                      child: Icon(
                        Icons.date_range,
                        color: _iconColor,
                        size: 20,
                      )),
                  Container(
                    child: FlatButton(
                      onPressed: () {
                        DatePicker.showDatePicker(
                          context,
                          showTitleActions: true,
                          minTime: DateTime(2020, 1, 1, 12, 00),
                          maxTime: DateTime(2025, 12, 12, 24, 00),
                          onChanged: (date) {},
                          onConfirm: (date) {
                            formattedDate =
                                DateFormat('yyyy-MM-dd').format(date);
                            setState(() {
                              _date = formattedDate;
                              _color = Colors.black38;
                              _iconColor = Colors.black38;
                              _widthBorder = 1.1;
                              _textColor = Colors.black54;
                              _check1 = _checks.where((check) {
                                var checkSuc = check.fecha
                                        .toLowerCase()
                                        .substring(0, 19 - 9) +
                                    check.sucursal.toLowerCase();
                                if (_sucursal == null) {
                                  _sucursal = "";
                                } else {
                                  _sucursal = _sucursal;
                                }
                                return checkSuc.contains(
                                    formattedDate.toLowerCase() + _sucursal);
                              }).toList();
                            });
                          },
                          currentTime: DateTime.now(),
                          locale: LocaleType.es,
                        );
                        setState(() {
                          _color = Colors.orange;
                          _iconColor = Colors.orange;
                          _widthBorder = 2.0;
                          _textColor = Colors.orange;
                        });
                      },
                      child: Text(
                        _date,
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            color: _textColor),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              height: 50,
              padding: EdgeInsets.only(left: 5, right: 10),
              child: TextField(
                cursorColor: Colors.orange,
                style: TextStyle(fontFamily: 'Lato'),
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(color: Colors.orange),
                    ),
                    prefixIcon: Icon(Icons.perm_identity),
                    labelText: 'Buscar responsable',
                    labelStyle: TextStyle(
                        fontFamily: 'Lato',
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold)),
                onChanged: (text) {
                  text = text.toLowerCase();
                  setState(() {
                    _check1 = _checks.where((check) {
                      var checkSuc = check.responsable.toLowerCase();
                      return checkSuc.contains(text);
                    }).toList();
                  });
                },
              ),
            ),
          ),
        ]),
      ],
    );
  }
}

class ItemTab {
  final String title;
  final IconData icon;
  const ItemTab({this.title, this.icon});
}

const List<ItemTab> choices = <ItemTab>[
  ItemTab(title: 'Del día', icon: Icons.access_time),
  ItemTab(title: 'Totales', icon: Icons.list),
];
