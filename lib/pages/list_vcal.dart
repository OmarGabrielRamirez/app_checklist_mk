import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

int criticosVpInt = 0,
    normalesVpInt = 0,
    criticosVpExt = 0,
    normalesVpExt = 0,
    totalNormales = 0,
    totalCriticos = 0,
    puntajeCritico = 0,
    puntajeNormal = 0,
    puntajePosible = 0,
    puntajeFinal = 0,
    count = 0;

double range1, range2 = 0.0;

double sizeFontTb = 20.0;

class ChecklistVPCal extends StatefulWidget {
  @override
  _checkListVpCal createState() => _checkListVpCal();
}

class _checkListVpCal extends State<ChecklistVPCal> {
  final jsonValCheckVpExt = {};
  var jsonValCheckVpInt;
  List valuesCheckVpExt = [];

  setValues() {
    criticosVpInt = 0;
    normalesVpInt = 0;
    criticosVpExt = 0;
    normalesVpExt = 0;
    totalNormales = 0;
    totalCriticos = 0;
    puntajeCritico = 0;
    puntajeNormal = 0;
    puntajePosible = 0;
    puntajeFinal = 0;
    count = 0;
  }

  getPuntajeFinally() async {
    var res = await http.get(
        Uri.encodeFull("https://intranet.prigo.com.mx/api/getcalvtp"),
        headers: {"Accept": "application/json"});
    var data = json.decode(res.body);
    for (var i in data) {
      puntajePosible += i['puntaje'];
      puntajePosible = puntajePosible;
    }

    range1 = puntajePosible / 2;
    range2 = puntajePosible / 3;

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    normalesVpInt = sharedPreferences.getInt("NormalesSlctVpInt");
    normalesVpExt = sharedPreferences.getInt("NormalesSlctVpExt");
    criticosVpInt = sharedPreferences.getInt("CriticosSlcVpInt");
    criticosVpExt = sharedPreferences.getInt("CriticosSlctVpExt");

    setState(() {
      totalCriticos = criticosVpInt + criticosVpExt;
      totalNormales = normalesVpInt + normalesVpExt;
      puntajeCritico = totalCriticos * 5;
      puntajeNormal = totalNormales * 2;
      int aux = puntajeCritico + puntajeNormal;
      puntajeFinal = puntajePosible - aux;
    });

    return puntajeFinal;
  }

  Icon setIcon(puntajeFinal) {
    if (puntajeFinal > 30) {
      return Icon(
        Icons.check_circle_outline,
        color: Colors.green,
        size: 88.0,
      );
    } else if (puntajeFinal > 20) {
      return Icon(
        Icons.check_circle_outline,
        color: Colors.orange,
        size: 88.0,
      );
    } else if (puntajeFinal < 20) {
      return Icon(Icons.close, color: Colors.red, size: 88.0);
    }
  }

  Text setText(puntajeFinal) {
    if (puntajeFinal > 30) {
      return Text("Satisfactorio",
          style:
              TextStyle(color: Colors.green, fontSize: 17, fontFamily: 'Lato', fontWeight: FontWeight.bold));
    } else if (puntajeFinal > 20) {
      return Text("No satisfactorio",
          style: TextStyle(
              color: Colors.orange, fontSize: 17, fontFamily: 'Lato', fontWeight: FontWeight.bold));
    } else if (puntajeFinal < 20) {
      return Text("Deficiente",
          style:
              TextStyle(color: Colors.red, fontSize: 17, fontFamily: 'Lato', fontWeight: FontWeight.bold, letterSpacing: .3));
    }
  }

  @override
  void initState() {
    this.getPuntajeFinally();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            actions: <Widget>[
              GestureDetector(
                  onTap: () {
                    setValues();
                    Navigator.popAndPushNamed(context, 'checklist_index');
                  },
                  child: Container(
                    padding: EdgeInsets.all(12),
                    alignment: Alignment.center,
                    child: Icon(
                      Icons.exit_to_app,
                      size: 25,
                    ),
                  )),
            ],
            leading: Icon(
              Icons.check,
              color: Colors.white,
              size: 16,
            ),
            iconTheme: IconThemeData(color: Colors.white),
            title: Text(
              "Calificación Vitrina Profunda",
              style: TextStyle(fontFamily: 'Lato', color: Colors.white, fontSize: 17),
            ),
          ),
          body: _content(),
        ),
        onWillPop: () async => false);
  }

  Widget _content() {
    return Container(
      alignment: Alignment.topCenter,
      padding: EdgeInsets.only(top: 40, left: 5, right: 5, bottom: 150),
      child: Container(
        padding: EdgeInsets.all(4),
        child: Column(
          children: <Widget>[
            Card(
              elevation: 7,
              child: DataTable(
                columns: [
                  DataColumn(
                    numeric: true,
                    label: Container(
                        alignment: Alignment.center,
                        child: Text(
                          "Puntaje",
                          style: TextStyle(
                              fontFamily: 'Lato',
                              fontSize: 25,
                              color: Colors.black,
                              fontWeight: FontWeight.w100),
                        )),
                  ),
                  DataColumn(
                      label: Text("Concepto",
                          style: TextStyle(
                              fontFamily: 'Lato',
                              fontSize: 25,
                              color: Colors.black,
                              fontWeight: FontWeight.w100)),
                      numeric: true),
                ],
                rows: [
                  DataRow(cells: [
                    DataCell(Text(
                      puntajePosible.toString(),
                      style: TextStyle(
                          fontSize: sizeFontTb,
                          color: Colors.black,
                          fontWeight: FontWeight.w300),
                    )),
                    DataCell(Text(
                      "Posibles",
                      style: TextStyle(
                          fontSize: sizeFontTb,
                          color: Colors.black,
                          fontWeight: FontWeight.w300),
                    )),
                  ]),
                  DataRow(cells: [
                    DataCell(Text(
                      puntajeCritico.toString(),
                      style: TextStyle(
                          fontSize: sizeFontTb,
                          color: Colors.red,
                          fontWeight: FontWeight.w300),
                    )),
                    DataCell(Text(
                      "Criticos",
                      style: TextStyle(
                          fontSize: sizeFontTb,
                          color: Colors.red,
                          fontWeight: FontWeight.w300),
                    )),
                  ]),
                  DataRow(cells: [
                    DataCell(Text(
                      puntajeNormal.toString(),
                      style: TextStyle(
                          fontSize: sizeFontTb,
                          color: Colors.orange,
                          fontWeight: FontWeight.w300),
                    )),
                    DataCell(Text(
                      "Normales",
                      style: TextStyle(
                          fontSize: sizeFontTb,
                          color: Colors.orange,
                          fontWeight: FontWeight.w300),
                    )),
                  ]),
                  DataRow(cells: [
                    DataCell(Text(
                      puntajeFinal.toString(),
                      style: TextStyle(
                          fontSize: sizeFontTb,
                          color: Colors.green,
                          fontWeight: FontWeight.w300),
                    )),
                    DataCell(
                      Text(
                        "Final",
                        style: TextStyle(
                            fontSize: sizeFontTb,
                            color: Colors.green,
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                  ]),
                ],
              ),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              child: _icon(),
            )
          ],
        ),
      ),
    );
  }

  Widget _icon() {
    return Container(
        alignment: Alignment.bottomCenter,
        margin: EdgeInsets.only(top: 10, left: 140, right: 140, bottom: 0),
        child: Column(
            children: <Widget>[
              Container(
                child: setIcon(puntajeFinal),
              ),
              Container(
                child:setText(puntajeFinal)
              )
            ]));
  }

  Widget _button() {
    return GestureDetector(
      onTap: () {
        setValues();
        Navigator.popAndPushNamed(context, 'checklist_index');
      },
      child: Container(
        height: 40,
        margin: EdgeInsets.symmetric(horizontal: 120),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50), color: Colors.orange),
        child: Center(
          child: Text(
            "Salir",
            style: TextStyle(
              fontFamily: 'Lato',
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
}
