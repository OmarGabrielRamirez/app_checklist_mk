import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';

class CheckListVitrinaPrImages extends StatefulWidget {
  @override
  _generarVitrinaPrImagePage createState() => _generarVitrinaPrImagePage();
}

class _generarVitrinaPrImagePage extends State<CheckListVitrinaPrImages> {
  String base64Image;
  final _comentariosController = TextEditingController();
  File _image1, _image2, _image3, _image4;
  String filename1, filename2, filename3, filename4;
  int idCheckF;
  static final url = "https://intranet.prigo.com.mx/storage/check/upload.php";

  saveData()async{
    print(_comentariosController.text+filename1+filename2+filename3+filename4+idCheckF.toString());
  }

  startUploadImage(String nameFile,File image, String filepst){
    String fileNameImage_= image.path.split("/").last;
    upload(fileNameImage_, nameFile, filepst);
  }

  upload(String fileNameImage, String nameFile, String filepost){
    setState(() {
      filepost = nameFile+"-"+idCheckF.toString()+"-"+"VP.jpg";
    });
    http.post(url, body: {
      "image": base64Image,
      "name": filepost,
    }).then((result){

    }).catchError((error){

    });
  }

  getPuntajeFinally() async {
    var res = await http.get(
        Uri.encodeFull("https://intranet.prigo.com.mx/api/getidfinally"),
        headers: {"Accept": "application/json"});
    var data = json.decode(res.body);
    for (var i in data) {
      setState(() {
        idCheckF = i['idCheckList'];
      });
    }

    return idCheckF;
  }

  Future _getImage1(File _img) async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      base64Image = base64Encode(image.readAsBytesSync());
    });
    setState(() {
      _image1 = image;
      startUploadImage( "Prueba 1", _image1, filename1);
    });
  }

  Future _getImage2(File _img) async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      base64Image = base64Encode(image.readAsBytesSync());
    });

    setState(() {
      _image2 = image;
      startUploadImage("Prueba 2",_image2, filename2);
    });
  }

  Future _getImage3(File _img) async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      base64Image = base64Encode(image.readAsBytesSync());
    });
    setState(() {
      _image3 = image;
      startUploadImage("Prueba 3",_image3, filename3);
    });
  }

  Future _getImage4(File _img) async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      base64Image = base64Encode(image.readAsBytesSync());
    });
    setState(() {
      _image4 = image;
      startUploadImage("Prueba 4",_image4, filename4);
    });
  }

  @override
  void initState() {
    super.initState();
    this.getPuntajeFinally();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          leading: Icon(Icons.camera_alt),
          iconTheme: IconThemeData(color: Colors.white),
          actions: <Widget>[
            GestureDetector(
                onTap: () {
                  saveData();
                  Navigator.pushNamed(context, 'calificacion_vp');
                },
                child: Container(
                  padding: EdgeInsets.all(12),
                  alignment: Alignment.center,
                  child: Icon(
                    Icons.arrow_forward,
                  ),
                )),
          ],
          title: Text(
            "Vitrina Profunda - Extras",
            style: TextStyle(
                fontFamily: 'Lato', color: Colors.white, fontSize: 17),
          ),
        ),
        body: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Card(
                  elevation: 4,
                  child: GridView.count(
                    padding: EdgeInsets.all(20),
                    shrinkWrap: true,
                    primary: false,
                    crossAxisCount: 2,
                    crossAxisSpacing: 5,
                    mainAxisSpacing: 5,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            left: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                            top: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                            right: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                            bottom: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                          ),
                        ),
                        child: Stack(
                          fit: StackFit.passthrough,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(top: 10),
                              alignment: Alignment.topCenter,
                              child: Text(
                                "Fotografía Prueba 1",
                                style: TextStyle(
                                    shadows: [
                                      Shadow(
                                        blurRadius: 1,
                                        color: Colors.black,
                                        offset: Offset(0.1, 0.1),
                                      ),
                                    ],
                                    color: Colors.orange,
                                    fontFamily: 'Lato',
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              alignment: Alignment.center,
                              child: _image1 == null
                                  ? IconButton(
                                      icon: Icon(Icons.add_a_photo,
                                          color: Colors.orange),
                                      onPressed: () {
                                        _getImage1(_image1);
                                      })
                                  : Stack(
                                      children: <Widget>[
                                        Container(
                                            padding: EdgeInsets.only(top: 25),
                                            width: 95,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(2.0),
                                              child: Image.file(_image1,
                                                  fit: BoxFit.scaleDown),
                                            )),
                                        Positioned(
                                          right: 1.0,
                                          bottom: 1.0,
                                          child: new IconButton(
                                            color: Colors.white,
                                            icon: Icon(
                                              Icons.replay,
                                              color: Colors.orange,
                                            ),
                                            onPressed: () {
                                              _getImage1(_image1);
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            left: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                            top: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                            right: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                            bottom: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                          ),
                        ),
                        child: Stack(
                          fit: StackFit.passthrough,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(top: 10),
                              alignment: Alignment.topCenter,
                              child: Text(
                                "Fotografía Prueba 2",
                                style: TextStyle(
                                    shadows: [
                                      Shadow(
                                        blurRadius: 1,
                                        color: Colors.black,
                                        offset: Offset(0.1, 0.1),
                                      ),
                                    ],
                                    color: Colors.orange,
                                    fontFamily: 'Lato',
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              alignment: Alignment.center,
                              child: _image2 == null
                                  ? IconButton(
                                      icon: Icon(Icons.add_a_photo,
                                          color: Colors.orange),
                                      onPressed: () {
                                        _getImage2(_image2);
                                      })
                                  : Stack(
                                      children: <Widget>[
                                        Container(
                                            padding: EdgeInsets.only(top: 25),
                                            width: 95,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(2.0),
                                              child: Image.file(_image2,
                                                  fit: BoxFit.scaleDown),
                                            )),
                                        Positioned(
                                          right: 1.0,
                                          bottom: 1.0,
                                          child: new IconButton(
                                            color: Colors.white,
                                            icon: Icon(
                                              Icons.replay,
                                              color: Colors.orange,
                                            ),
                                            onPressed: () {
                                              _getImage2(_image2);
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            left: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                            top: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                            right: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                            bottom: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                          ),
                        ),
                        child: Stack(
                          fit: StackFit.passthrough,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(top: 10),
                              alignment: Alignment.topCenter,
                              child: Text(
                                "Fotografía Prueba 3",
                                style: TextStyle(
                                    shadows: [
                                      Shadow(
                                        blurRadius: 1,
                                        color: Colors.black,
                                        offset: Offset(0.1, 0.1),
                                      ),
                                    ],
                                    color: Colors.orange,
                                    fontFamily: 'Lato',
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              alignment: Alignment.center,
                              child: _image3 == null
                                  ? IconButton(
                                      icon: Icon(Icons.add_a_photo,
                                          color: Colors.orange),
                                      onPressed: () {
                                        _getImage3(_image3);
                                      })
                                  : Stack(
                                      children: <Widget>[
                                        Container(
                                            padding: EdgeInsets.only(top: 25),
                                            width: 95,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(2.0),
                                              child: Image.file(_image3,
                                                  fit: BoxFit.scaleDown),
                                            )),
                                        Positioned(
                                          right: 1.0,
                                          bottom: 1.0,
                                          child: new IconButton(
                                            color: Colors.white,
                                            icon: Icon(
                                              Icons.replay,
                                              color: Colors.orange,
                                            ),
                                            onPressed: () {
                                              _getImage3(_image3);
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            left: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                            top: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                            right: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                            bottom: BorderSide(
                              color: Colors.orange,
                              width: 0.5,
                            ),
                          ),
                        ),
                        child: Stack(
                          fit: StackFit.passthrough,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(top: 10),
                              alignment: Alignment.topCenter,
                              child: Text(
                                "Fotografía Prueba 4",
                                style: TextStyle(
                                    shadows: [
                                      Shadow(
                                        blurRadius: 1,
                                        color: Colors.black,
                                        offset: Offset(0.1, 0.1),
                                      ),
                                    ],
                                    color: Colors.orange,
                                    fontFamily: 'Lato',
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              alignment: Alignment.center,
                              child: _image4 == null
                                  ? IconButton(
                                      icon: Icon(Icons.add_a_photo,
                                          color: Colors.orange),
                                      onPressed: () {
                                        _getImage4(_image4);
                                      })
                                  : Stack(
                                      children: <Widget>[
                                        Container(
                                            padding: EdgeInsets.only(top: 25),
                                            width: 95,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(2.0),
                                              child: Image.file(_image4,
                                                  fit: BoxFit.scaleDown),
                                            )),
                                        Positioned(
                                          right: 1.0,
                                          bottom: 1.0,
                                          child: new IconButton(
                                            color: Colors.white,
                                            icon: Icon(
                                              Icons.replay,
                                              color: Colors.orange,
                                            ),
                                            onPressed: () {
                                              _getImage4(_image4);
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                    padding: EdgeInsets.only(top: 1),
                    child: Card(
                      elevation: 4,
                      child: TextFormField(
                        controller: _comentariosController,
                        style: TextStyle(
                            fontFamily: 'Lato', fontWeight: FontWeight.bold),
                        keyboardType: TextInputType.multiline,
                        maxLines: 7,
                        decoration: InputDecoration(
                            labelText: 'Comentarios: ',
                            contentPadding: const EdgeInsets.all(10.0)),
                      ),
                    ))
              ],
            ),
          ),
        ]));
  }
}
