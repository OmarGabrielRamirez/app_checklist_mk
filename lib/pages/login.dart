import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;

  class LoginScreen extends StatefulWidget{
    @override
    _LoginScreenState createState() => _LoginScreenState();
  }

  notRotate(){
    SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,]);
  }

  class _LoginScreenState extends State<LoginScreen> {
    
    bool _isLoading = false; 
    
    @override
    Widget build(BuildContext context) {
    notRotate();
    return Scaffold(
      resizeToAvoidBottomInset:false,
        body:Container(
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              colors: [
                Colors.orange[900],
                Colors.orange[400]
              ]
            ),
          ),
          child: _isLoading ? Center(
            child: SpinKitFadingCircle( color: Colors.white,)):          
          Column(
            crossAxisAlignment:  CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox( height: 70),
              Padding(padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(horizontal:90, vertical: 40),
                      child:
                        Center(
                          child: Image.asset("assets/images/prigo_logo.png"),
                        )
                      
                    )
                  ],
                ),
              ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(60), topRight:Radius.circular(60) )
                ),
                child: Padding(
                  padding: EdgeInsets.all(30),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 40,),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: Colors.grey[200]
                                  ),
                                ),
                              ),
                          child: TextField(
                            style: TextStyle(fontFamily:'Lato', fontWeight: FontWeight.w600),
                            controller: _emailController,
                            cursorColor: Colors.orange,
                            decoration: InputDecoration(
                              hintStyle: TextStyle(fontFamily: 'Lato', fontWeight: FontWeight.w500, fontSize: 16),
                              labelStyle: TextStyle(fontFamily: 'Lato', fontWeight: FontWeight.w600, fontSize: 16),
                              labelText: "Email",
                              hintText: "ejemplo@prigo.com.mx",
                              border: InputBorder.none,
                              icon: Icon( Icons.email, size: 15.0, )
                            ),
                          ),
                            ),
                          ],
                        ),
                      ),
                    Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: Colors.grey[200]
                                  ),
                                ),
                              ),
                          child: TextField(
                            style: TextStyle(fontFamily:'Lato'),
                              obscureText: true,
                              cursorColor: Colors.orange,
                              controller: _passwordController,
                              decoration: InputDecoration(
                                labelStyle: TextStyle(fontFamily: 'Lato', fontWeight: FontWeight.w600, fontSize: 16),
                                labelText: "Contraseña",
                                border: InputBorder.none,
                                icon: Icon(Icons.vpn_key, size: 16.0, )
                              ),
                             ),
                            ),
                          ],
                        ),
                      ),
                    SizedBox(
                      height: 60,
                    
                    ),
                    GestureDetector(
                      onTap: (){
                      setState(() {
                      _isLoading = true;
                     });
                    signIn(_emailController.text, _passwordController.text, context);
                      },
                      child: Container(
                      height: 50, 
                      margin: EdgeInsets.symmetric(horizontal: 40),
                      decoration: BoxDecoration(
                        borderRadius:  BorderRadius.circular(50),
                        color: Colors.orange
                      ),
                         child: Center(
                          child: Text("Ingresar", style: TextStyle(fontFamily: 'Lato',color: Colors.white,  fontWeight: FontWeight.w600,),),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            ],
          ),
        ),
    );
  }

  void _showAlert(BuildContext context){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Icon(Icons.error_outline, color:Colors.red, size: 70.0),
            content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                   Text("Ingresa los datos correctamente para poder iniciar sesión.", style: TextStyle( fontWeight: FontWeight.bold, fontSize: 15.0, fontFamily: 'Lato')),     
                ]
              ),
              actions: <Widget>[
                FlatButton(onPressed: (){
                  Navigator.pop(context);
                }, child: Text("OK", style: TextStyle(color:Colors.orange),))
              ],
          );
        }
      );

    }

  signIn(String email, String password, BuildContext context) async {
    Map data = {
      'email': email,
      'password': password
    };

    var jsonData = null;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    
    var response = await http.post("https://intranet.prigo.com.mx/api/auth/login", body: data);

    if(response.statusCode == 200){
        jsonData = json.decode(response.body);
        if(jsonData != null){
          setState(() { _isLoading = false; });
        }
        sharedPreferences.setString("access_token", jsonData['access_token']);
       Navigator.pushReplacementNamed(context, '/');
    }else{
      setState(() {
      _isLoading = false;
      });
     _showAlert(context);
    }
  }

  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
}

