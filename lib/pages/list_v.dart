import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CheckListVpPage extends StatefulWidget {
  @override
  _checkListVpPage createState() => _checkListVpPage();
}

class _checkListVpPage extends State<CheckListVpPage> {
  List items = [];

  @override
  void iniState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(
          Icons.check,
          color: Colors.white,
          size: 16,
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.arrow_forward, color: Colors.white),
              onPressed: () {
                Navigator.pushNamed(context, 'check_vp_int');
              })
        ],
        title: Text(
          "Vitrina Profunda - Vitrina exterior",
          style: TextStyle(fontFamily: 'Lato', color: Colors.white),
        ),
      ),
      body: _content(),
    );
  }

  Widget _content() {
    return Stack(children: <Widget>[
      DataTable(
      columns: [
        DataColumn(
            label: Text(
          "Descripcion",
          style: TextStyle(
              fontFamily: 'Lato', fontSize: 18, fontWeight: FontWeight.bold),
        ))
      ],
      rows: List.generate(items.length, (index) => _getDataRow(items[index])),
    ),
      // _checkNormal(),
      // _checkCriticos(),
    ]);
  }

  Widget dataTableItems() {
    return DataTable(
      columns: [
        DataColumn(
            label: Text(
          "Descripcion",
          style: TextStyle(
              fontFamily: 'Lato', fontSize: 18, fontWeight: FontWeight.bold),
        ))
      ],
      rows: List.generate(items.length, (index) => _getDataRow(items[index])),
    );
  }
  
  DataRow _getDataRow(result) {
    return DataRow(cells: <DataCell>[
      DataCell(Text(result["descripcion"],
          style: TextStyle(fontFamily: 'Lato', fontSize: 18))),
    ]);
  }

  Future<String> getData() async {
    String urlGetData = "https://intranet.prigo.com.mx/api/getitemsvtpext";
    var res = await http.get(Uri.encodeFull(urlGetData),
        headers: {"Accept": "application/json"});
    var dataConvertedToJson = json.decode(res.body);
    setState(() {
      items = dataConvertedToJson['items_vitprofext'];
    });

    return "successful";
  }
}
