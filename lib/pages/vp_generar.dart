import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:http/http.dart' as http;

String date, time, suc, resp;

class GenerarVitrinaProPage extends StatefulWidget {
  @override
  _generarVitrinaProPage createState() => _generarVitrinaProPage();
}

class _generarVitrinaProPage extends State<GenerarVitrinaProPage> {
  String formattedDate;

  getResponsables(String seleccion) async {
    Map data = {'id': seleccion};

    var response = await http
        .post("https://intranet.prigo.com.mx/api/getresponsables", body: data);
    var resBody = json.decode(response.body);
    setState(() {
      dataResponsable = resBody;
    });
  }

  String _selection, _selectionR;

  final String urlGetData = "https://intranet.prigo.com.mx/api/checksucursales";

  List dataSucursales = List();
  List dataResponsable = List();

  Future<String> getData() async {
    var res = await http.get(Uri.encodeFull(urlGetData),
        headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      dataSucursales = resBody;
    });
  }

  saveData() async {
    var selectedDate = new DateTime.now();
    var formatter = new DateFormat('yyyy-dd-MM kk:mm');
    String date = formatter.format(selectedDate);
    if (date != null && _selection != null && _selectionR != null) {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      sharedPreferences.setString("dateUser", formattedDate);
      sharedPreferences.setString("dateAuto", date);
      sharedPreferences.setString("idSuc", _selection);
      sharedPreferences.setString("resp", _selectionR);
      _alertWar();
    }
  }

  @override
  void initState() {
    super.initState();
    this.getData();
    setState(() {
      DateTime now = DateTime.now();
      formattedDate = DateFormat('yyyy-dd-MM kk:mm').format(now);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white, size: 16, opacity: 0.9),
        title: Text("Nuevo - Vitrina Profunda",
            style: TextStyle(
                fontFamily: 'Lato',
                fontSize: 18,
                fontWeight: FontWeight.w500,
                color: Colors.white)),
      ),
      body: _content(),
    );
  }

  Widget _alertWar() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Icon(Icons.error_outline, color: Colors.orange, size: 70.0),
            content: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Text("Todos los datos ingresados, se guardarán.",
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 17.0,
                      fontFamily: 'Lato')),
            ]),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, 'check_vp');
                  },
                  child: Text(
                    "Continuar",
                    style: TextStyle(
                        color: Colors.orange,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Lato'),
                  )),
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    "Cancelar",
                    style: TextStyle(
                        color: Colors.orange,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Lato'),
                  ))
            ],
          );
        });
  }

  Widget _dropwRespon() {
    return Center(
      child: Column(children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 30, bottom: 10),
          child: Text("Gerente / Responsable",
              style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'Lato',
                  fontWeight: FontWeight.bold,
                  color: Colors.orange)),
        ),
        Container(
          decoration: BoxDecoration(
              border: Border.all(width: 0.9, color: Colors.orange),
              borderRadius: BorderRadius.all(Radius.circular(8.0))),
          child: DropdownButton(
            hint: Container(
                child: Row(
              children: <Widget>[
                SizedBox(width: 10),
                Icon(Icons.perm_identity, color: Colors.orange),
                SizedBox(width: 10),
                Text(
                  'Selecciona un responsable',
                  style: TextStyle(
                      fontFamily: 'Lato',
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(width: 10),
              ],
            )),
            underline: Container(color: Colors.white),
            icon: Icon(
              Icons.keyboard_arrow_down,
              color: Colors.orange,
            ),
            items: dataResponsable.map((item) {
              return DropdownMenuItem(
                  value: item['nombre'].toString(),
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: 10),
                      Icon(Icons.perm_identity,
                          size: 18.0, color: Colors.orange),
                      SizedBox(width: 10),
                      Text(
                        item['nombre'],
                        style: TextStyle(fontFamily: 'Lato', fontSize: 15.0),
                      ),
                      SizedBox(width: 10),
                    ],
                  ));
            }).toList(),
            onChanged: (newVal) {
              setState(() {
                _selectionR = newVal;
              });
            },
            value: _selectionR,
          ),
        )
      ]),
    );
  }

  Widget _dropwSuc() {
    return Center(
        child: Column(children: <Widget>[
      Container(
        padding: EdgeInsets.only(top: 0, bottom: 10),
        child: Text("Sucursal",
            style: TextStyle(
                fontSize: 16,
                fontFamily: 'Lato',
                fontWeight: FontWeight.bold,
                color: Colors.orange)),
      ),
      Container(
        decoration: BoxDecoration(
            border: Border.all(width: 0.9, color: Colors.orange),
            borderRadius: BorderRadius.all(Radius.circular(8.0))),
        child: DropdownButton(
          hint: Container(
              child: Row(
            children: <Widget>[
              SizedBox(width: 10),
              Icon(Icons.place, color: Colors.orange),
              SizedBox(width: 10),
              Text(
                'Selecciona una sucursal',
                style: TextStyle(
                    fontFamily: 'Lato',
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(width: 10),
            ],
          )),
          underline: Container(color: Colors.white),
          icon: Icon(
            Icons.keyboard_arrow_down,
            color: Colors.orange,
          ),
          items: dataSucursales.map((item) {
            return DropdownMenuItem(
                value: item['id'].toString(),
                child: Row(
                  children: <Widget>[
                    SizedBox(width: 10),
                    Icon(Icons.place, size: 18.0, color: Colors.orange),
                    SizedBox(width: 10),
                    Text(
                      item['idMicros'],
                      style: TextStyle(fontFamily: 'Lato', fontSize: 18.0),
                    ),
                  ],
                ));
          }).toList(),
          onChanged: (newVal) {
            setState(() {
              _selection = newVal;
              _selectionR = null;
              getResponsables(_selection);
            });
          },
          value: _selection,
        ),
      )
    ]));
  }

  Widget _buttonOk() {
    return GestureDetector(
      onTap: () {
        saveData();
      },
      child: Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 80),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50), color: Colors.orange),
        child: Center(
          child: Text(
            "Continuar",
            style: TextStyle(
              fontFamily: 'Lato',
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }

  Widget _dataPicker() {
    return Container(
        padding: EdgeInsets.only(top: 30, bottom: 20),
        alignment: Alignment.topCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.date_range,
              color: Colors.orange,
            ),
            FlatButton(
              onPressed: () {
                DatePicker.showDateTimePicker(
                  context,
                  showTitleActions: true,
                  minTime: DateTime.now(),
                  maxTime: DateTime(2025, 12, 12, 24, 00),
                  onChanged: (date) {},
                  onConfirm: (date) {
                    setState(() {
                      formattedDate =
                          DateFormat('yyyy-dd-MM kk:mm').format(date);
                    });
                  },
                  currentTime: DateTime.now(),
                  locale: LocaleType.es,
                );
              },
              child: Text(
                formattedDate,
                style: TextStyle(
                  color: Colors.orange,
                  fontFamily: 'Lato',
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ));
  }

  Widget _content() {
    return Container(
      margin: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 40),
      child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 1.0,
          child: ListView(
            children: <Widget>[
              _dataPicker(),
              _dropwSuc(),
              _dropwRespon(),
              SizedBox(
                height: 40,
              ),
              _buttonOk()
            ],
          )),
    );
  }
}
