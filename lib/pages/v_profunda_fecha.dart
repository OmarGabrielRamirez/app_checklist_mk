import 'package:app_operativa/models/check_model.dart';
import 'package:app_operativa/pages/v_profunda.dart';
import 'package:app_operativa/pages/details_vp.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class VitrinaProPageFecha extends StatefulWidget {
  @override
  _vProfundaPage createState() => _vProfundaPage();
}

class _vProfundaPage extends State<VitrinaProPageFecha> {
  List<Check> _checks = List<Check>();
  List<Check> _check1 = List<Check>();

  Future<List<Check>> getCheck() async {
    var url = "https://intranet.prigo.com.mx/api/getcheckvp";
    var response = await http.get(url);
    var allChecks = List<Check>();

    if (response.statusCode == 200) {
      var checks = json.decode(response.body);
      for (var check in checks) {
        allChecks.add(Check.fromJson(check));
      }
    }
    return allChecks;
  }

  Icon setIcon(puntajeFinal) {
    if (puntajeFinal > 30) {
      return Icon(
        Icons.check_circle_outline,
        color: Colors.green,
        size: 38.0,
      );
    } else if (puntajeFinal > 20) {
      return Icon(
        Icons.check_circle_outline,
        color: Colors.orange,
        size: 38.0,
      );
    } else if (puntajeFinal < 20) {
      return Icon(Icons.close, color: Colors.red, size: 38.0);
    }
  }

  Text setPuntaje(puntajeFinal) {
    if (puntajeFinal > 30) {
      return Text(
        puntajeFinal.toString() + " / 48",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
            color: Colors.green,
            fontFamily: 'lato'),
      );
    } else if (puntajeFinal > 20) {
      return Text(
        puntajeFinal.toString() + " / 48",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
            color: Colors.orange,
            fontFamily: 'lato'),
      );
    } else if (puntajeFinal < 20) {
      return Text(
        puntajeFinal.toString() + " / 48",
        style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.bold,
            color: Colors.red,
            fontFamily: 'lato'),
      );
    }
  }

  @override
  void initState() {
    getCheck().then((value) {
      setState(() {
        _checks.addAll(value);
        _check1 = _checks;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: _content(),
      appBar: AppBar(
          title: Text("Vitrina Profunda",
              style: TextStyle(
                  fontFamily: 'Lato',
                  fontWeight: FontWeight.w500,
                  fontSize: 17,
                  color: Colors.white)),
          iconTheme:
              IconThemeData(color: Colors.white, size: 16, opacity: 0.9)),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, 'n_vprofunda');
          },
          backgroundColor: Colors.orange,
          child: Icon(Icons.add, size: 30, color: Colors.white)),
    );
  }

  Widget _content() {
    return ListView.builder(
        padding: EdgeInsets.symmetric(vertical: 1, horizontal: 10),
        itemCount: _check1.length + 1,
        itemBuilder: (context, index) {
          return index == 0 ? _searchBar() : _listVolumios(index - 1);
        });
  }

  _listVolumios(index) {
    String fecha = _check1[index].fecha.toString();
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => DetailsPage(_check1[index].id.toString())));
      },
      child: Card(
        color: Colors.white,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        elevation: 10.0,
        child: Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                ListTile(
                  leading: Container(
                    padding: EdgeInsets.only(top: 20),
                    child: setIcon(_check1[index].puntajeFinal),
                  ),
                  dense: true,
                  title: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          _check1[index].sucursal,
                          style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 19,
                            fontWeight: FontWeight.w600,
                            color: Colors.orange,
                            shadows: [
                              Shadow(
                                blurRadius: 0.1,
                                color: Colors.black,
                                offset: Offset(0.1, 0.5),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  subtitle: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 5, right: 5),
                            alignment: Alignment.centerLeft,
                            child: Icon(Icons.perm_identity,
                                size: 17, color: Colors.orange),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5),
                            alignment: Alignment.centerLeft,
                            child: Text(_check1[index].responsable,
                                style: TextStyle(
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.w200,
                                    color: Colors.white,
                                    shadows: [
                                      Shadow(
                                        blurRadius: 0.4,
                                        color: Colors.black,
                                        offset: Offset(1.0, 1.0),
                                      ),
                                    ],
                                    fontFamily: 'Lato')),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 5, right: 5),
                            alignment: Alignment.centerLeft,
                            child: Icon(Icons.date_range,
                                size: 17, color: Colors.orange),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5),
                            alignment: Alignment.centerLeft,
                            child: Text(fecha.substring(0, 18 - 2),
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.black,
                                    shadows: [
                                      Shadow(
                                        blurRadius: 1.5,
                                        color: Colors.white,
                                        offset: Offset(1.0, 0.5),
                                      ),
                                    ],
                                    fontFamily: 'Lato')),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 5, right: 5),
                            alignment: Alignment.centerLeft,
                            child: Icon(Icons.assessment,
                                size: 17, color: Colors.orange),
                          ),
                          Container(
                              padding: EdgeInsets.only(top: 4),
                              child: setPuntaje(_check1[index].puntajeFinal))
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }

  Future<void> _showSelectionImage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Container(
                child: Row(
              children: <Widget>[
                Container(
                    padding: EdgeInsets.only(right: 10),
                    child: Icon(
                      Icons.search,
                      size: 18,
                      color: Colors.orange,
                    )),
                Container(
                    child: Text("Busqueda avanzada",
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 20,
                            fontWeight: FontWeight.w300)))
              ],
            )),
            content: SingleChildScrollView(
                child: ListBody(children: <Widget>[
              GestureDetector(
                child: Center(
                    child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      child: Icon(
                        Icons.date_range,
                        size: 15,
                        color: Colors.orange,
                      ),
                    ),
                    Text("Fecha",
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.orange))
                  ],
                )),
                onTap: () {
                  print("Fecha");
                },
              ),
              Padding(padding: EdgeInsets.only(top: 10)),
              GestureDetector(
                child: Center(
                    child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      child: Icon(
                        Icons.perm_identity,
                        size: 15,
                        color: Colors.orange,
                      ),
                    ),
                    Text("Responsable",
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.orange))
                  ],
                )),
                onTap: () {
                  print("Responsable");
                },
              ),
              Padding(padding: EdgeInsets.only(top: 10)),
              GestureDetector(
                child: Center(
                    child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      child: Icon(
                        Icons.place,
                        size: 15,
                        color: Colors.orange,
                      ),
                    ),
                    Text("Sucursal",
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.orange))
                  ],
                )),
                onTap: () {

                },
              ),
            ])),
          );
        });
  }

  _searchBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: TextField(
        cursorColor: Colors.orange,
        style: TextStyle(fontFamily: 'Lato'),
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.search),
            suffixIcon: IconButton(
                icon: Icon(Icons.more_vert),
                onPressed: () {
                  _showSelectionImage();
                }),
            labelText: 'Buscar por fecha',
            hintText: 'K Manacar',
            hintStyle: TextStyle(
              fontFamily: 'Lato',
              fontSize: 14.0,
            ),
            labelStyle: TextStyle(
                fontFamily: 'Lato',
                fontSize: 14.0,
                fontWeight: FontWeight.bold)),
        onChanged: (text) {
          text = text.toLowerCase();
          setState(() {
            _check1 = _checks.where((check) {
              var checkSuc = check.sucursal.toLowerCase();
              return checkSuc.contains(text);
            }).toList();
          });
        },
      ),
    );
  }
}

class VitrinaProPage {
}
