import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class DetailsPage extends StatefulWidget {
  final String idCheck;
  DetailsPage(this.idCheck);
  @override
  _detailsPage createState() => _detailsPage();
}

class _detailsPage extends State<DetailsPage> {
  List items = [];
  String _idCheck;
  @override
  void initState() {
    super.initState();

    setState(() {
      _idCheck = widget.idCheck;
    });
  }

    Icon setIcon(puntajeFinal) {
    if (puntajeFinal > 30) {
      return Icon(
        Icons.check_circle_outline,
        color: Colors.green,
        size: 80.0,
      );
    } else if (puntajeFinal > 20) {
      return Icon(
        Icons.check_circle_outline,
        color: Colors.orange,
        size: 80.0,
      );
    } else if (puntajeFinal < 20) {
      return Icon(Icons.close, color: Colors.red, size: 80.0);
    }
  }

  Text setText(puntajeFinal) {
    if (puntajeFinal > 30) {
      return Text("Satisfactorio",
          style:
              TextStyle(color: Colors.green, fontSize: 20, fontFamily: 'Lato'));
    } else if (puntajeFinal > 20) {
      return Text("No satisfactorio",
          style: TextStyle(
              color: Colors.orange, fontSize: 20, fontFamily: 'Lato'));
    } else if (puntajeFinal < 20) {
      return Text("Deficiente",
          style:
              TextStyle(color: Colors.red, fontSize: 20, fontFamily: 'Lato'));
    }
  }

  Future<List<Item>> getData(String idCheck) async {
    Map dataPost = {'idCheck': idCheck};
    var response = await http.post(
        "https://intranet.prigo.com.mx/api/getdetailsvpext",
        body: dataPost);
    var data = json.decode(response.body);
    List<Item> items = [];
    for (var i in data) {
      Item item = Item(i["idPregunta"], i["accion"], i["critico"], i["estado"]);
      items.add(item);
    }
    return items;
  }

  Future<List<Item>> getDataInt(String idCheck) async {
    Map dataPost = {'idCheck': idCheck};
    var response = await http.post(
        "https://intranet.prigo.com.mx/api/getdetailsvpint",
        body: dataPost);
    var data = json.decode(response.body);
    List<Item> items = [];
    for (var i in data) {
      Item item = Item(i["idPregunta"], i["accion"], i["critico"], i["estado"]);
      items.add(item);
    }
    return items;
  }

  Future<List<CheckListo>> getInfo(String idCheck) async {
    Map dataPost = {'idCheck': idCheck};
    var response = await http
        .post("https://intranet.prigo.com.mx/api/getinfocheck", body: dataPost);
    var data = json.decode(response.body);
    List<CheckListo> checks = [];
    for (var i in data) {
      CheckListo check = CheckListo(
          i["responsable"],
          i["fechaIngresada"],
          i["puntajeFinal"],
          i["puntajeCritico"],
          i["puntajeNormal"],
          i["nombre"]);
      checks.add(check);
    }
    return checks;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GridView.count(
        crossAxisCount: 1,
        crossAxisSpacing: 10.0,
        childAspectRatio: 1.0,
        mainAxisSpacing: 10.0,
        padding: EdgeInsets.all(20),
        children: <Widget>[_cardInfo(), _content(), _contentInt()],
      ),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(
          "Detalles Checklist - Vitrina Profunda",
          style: TextStyle(
            fontSize: 17,
              fontFamily: 'Lato',
              fontWeight: FontWeight.w500,
              color: Colors.white),
        ),
      ),
      
    );
  }

  Widget _content() {
    return Stack(
      children: <Widget>[
        _cardVpExt(),
      ],
    );
  }

  Widget _contentInt() {
    return Stack(
      children: <Widget>[
        _cardVpInt(),
      ],
    );
  }

  Widget _txtVpExt() {
    return Container(
      padding: EdgeInsets.only(top: 20),
      alignment: Alignment.topCenter,
      child: Text(
        "Vitrina Profunda Exterior",
        style: TextStyle(
            fontFamily: 'Lato',
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.black45),
      ),
    );
  }

  Widget _txtVpInt() {
    return Container(
      padding: EdgeInsets.only(top: 20),
      alignment: Alignment.topCenter,
      child: Text(
        "Vitrina Profunda Interior",
        style: TextStyle(
            fontFamily: 'Lato',
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.black45),
      ),
    );
  }

  Widget _cardVpInt() {
    return Container(
      child: Card(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        elevation: 3.0,
        child: Stack(children: <Widget>[_txtVpInt(), _detailsVpInt()]),
      ),
    );
  }

  Widget _cardInfo() {
    return Container(
      child: Card(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        elevation: 3.0,
        child: Stack(children: <Widget>[_infoCheck()]),
      ),
    );
  }

  Widget _cardVpExt() {
    return Container(
      child: Card(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        elevation: 3.0,
        child: Stack(
          children: <Widget>[_txtVpExt(), _detailsVpExt()],
        ),
      ),
    );
  }

  Widget _detailsVpInt() {
    return Container(
      padding: EdgeInsets.only(top: 50, left: 20, right: 20, bottom: 20),
      child: FutureBuilder(
          future: getDataInt(_idCheck),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                  child: Center(
                child: Text("Cargando...",
                    style: TextStyle(
                        fontFamily: 'Lato',
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.black45)),
              ));
            } else {
              Widget icon;
              Widget iconT;
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (snapshot.data[index].critico == 1) {
                      icon = Icon(
                        Icons.assistant_photo,
                        size: 17,
                        color: Colors.red,
                      );
                    } else if (snapshot.data[index].critico == 0) {
                      icon = Icon(
                        Icons.assistant_photo,
                        size: 17,
                        color: Colors.orange,
                      );
                    }

                    if (snapshot.data[index].status == 1) {
                      iconT = Icon(
                        Icons.check,
                        color: Colors.green,
                        size: 25,
                      );
                    } else if (snapshot.data[index].status == 0) {
                      iconT = Icon(
                        Icons.close,
                        size: 25,
                        color: Colors.red,
                      );
                    }
                    return ListTile(
                      leading: icon,
                      title: Text(
                        snapshot.data[index].descripcion,
                        style: TextStyle(
                          fontFamily: 'Lato',
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      trailing: iconT,
                    );
                  });
            }
          }),
    );
  }

  Widget _detailsVpExt() {
    return Container(
      padding: EdgeInsets.only(top: 50, left: 20, right: 20, bottom: 20),
      child: FutureBuilder(
          future: getData(_idCheck),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                  child: Center(
                child: Text("Cargando...",
                    style: TextStyle(
                        fontFamily: 'Lato',
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.black45)),
              ));
            } else {
              Widget icon;
              Widget iconT;
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (snapshot.data[index].critico == 1) {
                      icon = Icon(
                        Icons.assistant_photo,
                        size: 17,
                        color: Colors.red,
                      );
                    } else if (snapshot.data[index].critico == 0) {
                      icon = Icon(
                        Icons.assistant_photo,
                        size: 17,
                        color: Colors.orange,
                      );
                    }

                    if (snapshot.data[index].status == 1) {
                      iconT = Icon(
                        Icons.check,
                        color: Colors.green,
                        size: 25,
                      );
                    } else if (snapshot.data[index].status == 0) {
                      iconT = Icon(
                        Icons.close,
                        size: 25,
                        color: Colors.red,
                      );
                    }
                    return ListTile(
                      leading: icon,
                      title: Text(
                        snapshot.data[index].descripcion,
                        style: TextStyle(
                          fontFamily: 'Lato',
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      trailing: iconT,
                    );
                  });
            }
          }),
    );
  }

  Widget _infoCheck() {
    return Container(
      padding: EdgeInsets.only(top: 5, left: 20, right: 20, bottom: 10),
      child: FutureBuilder(
          future: getInfo(_idCheck),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                  child: Center(
                child: Text("Cargando...",
                    style: TextStyle(
                        fontFamily: 'Lato',
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.black45)),
              ));
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      child: Column(children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 20),
                          child: Text(
                            snapshot.data[index].sucursal,
                            style: TextStyle(
                                fontFamily: 'Lato',
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                                color: Colors.orange),
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(left: 20, right: 10),
                                  child: Icon(
                                    Icons.perm_identity,
                                    size: 20,
                                    color: Colors.orange,
                                  ),
                                ),
                                Text(
                                  snapshot.data[index].responsable,
                                  style: TextStyle(
                                      fontFamily: 'Lato',
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black45),
                                )
                              ],
                            )),
                        Container(
                            padding: EdgeInsets.only(top: 5),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(left: 20, right: 10),
                                  child: Icon(
                                    Icons.date_range,
                                    size: 20,
                                    color: Colors.orange,
                                  ),
                                ),
                                Text(
                                  snapshot.data[index].fecha.substring(0, 18 - 2),
                                  style: TextStyle(
                                      fontFamily: 'Lato',
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black45),
                                )
                              ],
                            )),
                        Container(
                            padding: EdgeInsets.only(top: 5),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(left: 20, right: 10),
                                  child: Icon(
                                    Icons.error_outline,
                                    size: 20,
                                    color: Colors.red,
                                  ),
                                ),
                                Text(
                                  snapshot.data[index].puntajeCritico
                                      .toString(),
                                  style: TextStyle(
                                      fontFamily: 'Lato',
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.red),
                                ),
                                Text(
                                  "  Puntaje Critico Restado",
                                  style: TextStyle(
                                      fontFamily: 'Lato',
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.red),
                                )
                              ],
                            )),
                        Container(
                            padding: EdgeInsets.only(top: 5),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(left: 20, right: 10),
                                  child: Icon(
                                    Icons.error_outline,
                                    size: 20,
                                    color: Colors.orange,
                                  ),
                                ),
                                Text(
                                  snapshot.data[index].puntajeNormal.toString(),
                                  style: TextStyle(
                                      fontFamily: 'Lato',
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.orange),
                                ),
                                Text(
                                  "  Puntaje Normal Restado",
                                  style: TextStyle(
                                      fontFamily: 'Lato',
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.orange),
                                )
                              ],
                            )),
                        Container(
                            padding: EdgeInsets.only(top: 5),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(left: 20, right: 10),
                                  child: Icon(
                                    Icons.assessment,
                                    size: 20,
                                    color: Colors.orange,
                                  ),
                                ),
                                Text(
                                  snapshot.data[index].puntajeFinal.toString() +
                                      " / 48",
                                  style: TextStyle(
                                      fontFamily: 'Lato',
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black45),
                                ),
                                Text(
                                  "  Puntaje Final",
                                  style: TextStyle(
                                      fontFamily: 'Lato',
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black45),
                                )
                              ],
                            )),
                          Container(
                            padding: EdgeInsets.only(top:10),
                            child:Column(children: <Widget>[
                              setIcon(snapshot.data[index].puntajeFinal),
                              setText(snapshot.data[index].puntajeFinal)
                            ],)
                          )
                      ]),
                    );
                  });
            }
          }),
    );
  }
}

class Item {
  final int idItem;
  final int status;
  final String descripcion;
  final int critico;

  Item(this.idItem, this.descripcion, this.critico, this.status);
}

class CheckListo {
  final String responsable;
  final String fecha;
  final int puntajeFinal;
  final int puntajeCritico;
  final int puntajeNormal;
  final String sucursal;

  CheckListo(this.responsable, this.fecha, this.puntajeFinal,
      this.puntajeCritico, this.puntajeNormal, this.sucursal);
}
