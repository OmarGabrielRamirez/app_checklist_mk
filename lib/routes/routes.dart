import 'package:app_operativa/pages/list_vcal.dart';
import 'package:app_operativa/pages/listvp_images.dart';
import 'package:app_operativa/pages/vit_profu_index.dart';
import 'package:flutter/material.dart';
import '../main.dart';


import 'package:app_operativa/pages/checklist_index.dart';
import 'package:app_operativa/pages/list_vint.dart';
import 'package:app_operativa/pages/list_vtproext.dart';
import 'package:app_operativa/pages/login.dart';
import 'package:app_operativa/pages/v_profunda.dart';
import 'package:app_operativa/pages/vp_generar.dart';


      Map <String, WidgetBuilder> getApplicationRoutes(){
        return <String, WidgetBuilder>{
            '/': (BuildContext context) =>  MainPage(),
            'login' : (BuildContext context) => LoginScreen(),
            'checklist_index': (BuildContext context) => ChecklistPage(),
            'v_profunda' : (BuildContext context) => VitrinaProPageIndex(),
            'n_vprofunda': (BuildContext context) => GenerarVitrinaProPage(),
            'check_vp' : (BuildContext context) => CheckListVitrinaPrExt(),
            'check_vp_int' : (BuildContext context) => CheckListVpIntePage(),
            'calificacion_vp' : (BuildContext context) => ChecklistVPCal(),
            'images_vp' : (BuildContext context) => CheckListVitrinaPrImages()
        };
      }