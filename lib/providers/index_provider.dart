
import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;

class _MenuProvider{

  List <dynamic> itemsMenu = [];

  Future<List<dynamic>> loadingData() async{
    final resp = await rootBundle.loadString('data/index_opt.json');
    Map dataMap = json.decode(resp);
    itemsMenu = dataMap['routes'];
    return itemsMenu;
  } 

  Future<List<dynamic>> loadingDataCheck() async{
    final resp = await rootBundle.loadString('data/index_check.json');
    Map dataMap = json.decode(resp);
    itemsMenu = dataMap['routes'];
    return itemsMenu;
  } 
  
}

final MenuProvider = new _MenuProvider();