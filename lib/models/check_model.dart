class Check{
  int id;
  String responsable;
  String fecha;
  int puntajeFinal;
  int puntajeCritico;
  int puntajeNormal;
  String sucursal;

  Check({this.id,this.responsable, this.fecha, this.puntajeFinal, this.puntajeCritico, this.puntajeNormal});

  Check.fromJson(Map<String, dynamic>json){
      id = json['idCheckList'];
      responsable = json['responsable'];
      fecha = json['fechaIngresada'];
      puntajeFinal = json['puntajeFinal'];
      puntajeCritico = json['puntajeCritico'];
      puntajeNormal = json['puntajeNormal'];
      sucursal = json['nombre'];
  }
}

