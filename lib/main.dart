import 'package:app_operativa/providers/index_provider.dart';
import 'package:app_operativa/routes/routes.dart';
import 'package:app_operativa/utils/icon_string_util.dart';
import 'package:app_operativa/utils/shadow_string_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Prigo Operativo',
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: getApplicationRoutes(),
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
    );
  }
}

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

notRotate() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
}

class _MainPageState extends State<MainPage> {
  
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("access_token") == null) {
      Navigator.pushNamed(context, 'login');
    }
  }

  Future logOut(BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.getKeys();
    for (String _key in sharedPreferences.getKeys()) {
      if (_key == "access_token") {
        sharedPreferences.remove(_key);
        Navigator.pushReplacementNamed(context, 'login');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    notRotate();
    return Scaffold(
      body: Container(
        constraints: new BoxConstraints.tightFor(),
        color: Colors.white,
        child: Stack(
          children: <Widget>[_background(), _getGradient(), _items()],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.orange,
        child: Icon(Icons.power_settings_new, color: Colors.white),
        onPressed: () {
          logOut(context);
        },
      ),
    );
  }

  Container _background() {
    return Container(
      child: Image.asset(
        "assets/images/kayser.jpg",
        fit: BoxFit.cover,
        height: 300.0,
      ),
    );
  }

  Container _getGradient() {
    return Container(
        margin: EdgeInsets.only(top: 190.0),
        height: 110.0,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[Color(0x00FFFFFF), Color(0xFFFFFFFF)],
            stops: [0.0, 0.9],
            begin: const FractionalOffset(0.0, 0.0),
            end: const FractionalOffset(0.0, 1.0),
          ),
        ));
  }

  Widget _items() {
    return FutureBuilder(
      future: MenuProvider.loadingData(),
      initialData: [],
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          padding: EdgeInsets.fromLTRB(24.0, 140, 24.0, 32.0),
          children: _itemsList(snapshot.data, context),
        );
      },
    );
  }

  List<Widget> _itemsList(List<dynamic> data, BuildContext context) {
    final List<Widget> itmMenu = [];
    data.forEach((opt) {
      final widgetTmp = GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, opt['route']);
        },
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          margin: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 13, right: 13),
          elevation: 5.0,
          child: Container(
            margin: EdgeInsets.only(top: 30.0, bottom: 30.0),
            child: Column(children: <Widget>[
              Text(opt['title'],
                  style: TextStyle(
                      fontFamily: 'Lato',
                      color: Colors.orange,
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold)),
              SizedBox(
                width: 20.0,
                height: 10.0,
              ),
              Stack(
                children: <Widget>[
                  Positioned(
                    left: 1.0,
                    top: 2.0,
                    child: getShadow(opt['icon']),
                  ),
                  getIcon(opt['icon']),
                ],
              ),
            ]),
          ),
        ),
      );
      itmMenu..add(widgetTmp);
    });
    return itmMenu;
  }
}
